var net = require("net");
var JSONStream = require('JSONStream');
var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var infoInit = null; //objeto que contendra el data.data del gameInit
var aux2; //infoInit.race.track
var le; //Número de piezas
var infoTurbo = null;
var turboAvailable = false;
var myCarColor = 'none'; //color de nuestro coche COMMENTS FTW PLS
var myCar = 0; //Indice de nuestro coche en el vector de coches

var x_before;
var x_now;
var x_ini_cs;
var x_fin_ca;
var switched = 0;
var velsw;

var crashed = false; // true si nos la hemos pegado, false si todo ok. Cuando hay un spawn se pone a false otra vez

var switchToLane = ""; //{"Left","","Right"}
var isUsefullSwitch = true; //para evitar el desperdicio de tiempo de calculo si ya hemos calculado el siguiente switch
var nextSwitch_real;
var rectaGrande = true;
var max_long_zona_turbo = 0.0;
var velocitat;

var midiendo = 1; //VARIABLES MARAH CALCULOS 
var lambda = 5;
var k = 1;
var k1 = 0.11111111111518157;
var k2 = -0.0013888888895356502;
var b = 0.0013888888895356502;
var c1 =  -0.33333333333307974;
var c2 = 0.59;
var c2provi = true;

var e = Math.E;
var dis_next_curve = 0; //VARIABLES MARAH THROTTLE
var calcR = false;
var calcC = false;
var calcS = true;
var v_max_rect;
var dist_f_rect;
var dist_frenado;
var frenar_cs = false;
var curve_size = 10000;
var ang_anterior;
var ang_previo;

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);
client = net.connect(serverPort, serverHost, function () {
    return send({
        msgType: "join",
        data: {
                name: botName,
                key: botKey
        }
    });//"germany usa keimola france elaeintarha imola england suzuka"
});
function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
};

function long(x1, x2) {
    var dist = x2.dist - x1.dist;
    var i = x1.pieceIndex;
    while ((i%le) != x2.pieceIndex) {
        if (aux2.pieces[i%le].hasOwnProperty("length")) dist += aux2.pieces[i%le].length;
        else {
            var curva = aux2.pieces[i%le]
            dist += Math.PI * Math.abs(curva.angle) / 180 * (curva.radius - (curva.angle/Math.abs(curva.angle))*aux2.lanes[x1.lane].distanceFromCenter);
        }
        ++i;
    }
    return dist;
};

function velocity(data) {
    var v = new Array(infoInit.race.cars.length);
    for(var i = 0; i < infoInit.race.cars.length; i++) {
        x_now[i] = {
            pieceIndex: data.data[i].piecePosition.pieceIndex,
            lane: data.data[i].piecePosition.lane.startLaneIndex,
            dist: data.data[i].piecePosition.inPieceDistance
        };
        if(switched[i] && !aux2.pieces[x_now[i].pieceIndex].hasOwnProperty("switch")) {
            switched[i] = false;
            v[i] = velsw[i];
        }
        else v[i] = long(x_before[i], x_now[i]);
        x_before[i] = {
            pieceIndex: x_now[i].pieceIndex,
            lane: x_now[i].lane,
            dist: x_now[i].dist
        }
    }
    return v;
};

function switchLane(data,postIndex, aux,velm) {
    var nextSwitch = postIndex;
	nextSwitch = (nextSwitch + 1) % le;
	var x1 = {
        pieceIndex: nextSwitch,
        lane: data.data[myCar].piecePosition.lane.startLaneIndex,
        dist: 0
    };
	while (!aux2.pieces[nextSwitch].hasOwnProperty("switch") && nextSwitch !== x_now[myCar].pieceIndex) {
        nextSwitch = (nextSwitch + 1) % le;
    }
    var x2 = {
        pieceIndex: nextSwitch,
        lane: x1.lane,
        dist: 0
    };
    return useful(data, x1, x2, aux,velm);
};

function useful(data, x1, x2, aux,velm) {
var v = [{dir:"left", dist: Infinity, ocup: false, vel: Infinity}, {dir: "act", dist: long(x1,x2), ocup: false, vel: Infinity}, {dir: "right", dist: Infinity, ocup: false, vel: Infinity}];
    var postSwitch = (aux2.pieces[(x_now[myCar].pieceIndex + 2) % le]);
    for(var i = 0; i < data.data.length; i++) {
        var otroCoche = data.data[i].piecePosition;
        var dist_detras = long({pieceIndex: otroCoche.pieceIndex, lane: x_now[myCar].lane, dist: otroCoche.inPieceDistance}, x_now[myCar]);
        var dist_frente = long(x_now[myCar], {pieceIndex: otroCoche.pieceIndex, lane: x_now[myCar].lane, dist: otroCoche.inPieceDistance});
        console.log("dist_detras: " + dist_detras + ", dist_frente: " + dist_frente);
        if(i != myCar && (dist_detras < 0.3*k || dist_frente < 8*k)) {
            console.log("ILLO QUE NOS CHOCAMOS CON ESE " + infoInit.race.cars[i].id.color + ", nuestra v: " + aux[myCar] + ", su v: " + aux[i]);
            if(otroCoche.lane.endLaneIndex == x_now[myCar].lane-1 && aux[i] < v[0].vel && aux[i]/velm < 0.8) { v[0].ocup = true; v[0].vel = aux[i]; }
            else if(otroCoche.lane.endLaneIndex == x_now[myCar].lane && aux[i] < v[1].vel && aux[i]/velm < 0.8) { v[1].ocup= true; v[1].vel = aux[i]; }
            else if(otroCoche.lane.endLaneIndex == x_now[myCar].lane+1 && aux[i] < v[2].vel && aux[i]/velm < 0.8) { v[2].ocup= true; v[2].vel = aux[i]; }
        }
    }
    var borde_derecho = false;
    var borde_izquierdo = false;
    if(x1.lane + 1 >= aux2.lanes.length) { //Estamos a la derecha del todo, solo podemos switchear a la izquierda
        borde_derecho = true;
        v[2].ocup = true;
        v[2].vel = 0;
    }
    if(x1.lane - 1 < 0) { //Estamos a la izquierda del todo, solo podemos switchear a la derecha
        borde_izquierdo = true;
        v[0].ocup = true;
        v[0].vel = 0;
    }
    if(!borde_derecho) {
        var x3 = {
            pieceIndex: x1.pieceIndex,
            lane: x1.lane + 1,
            dist: x1.dist
        };
        var x4 = {
            pieceIndex: x2.pieceIndex,
            lane: x2.lane + 1,
            dist: x2.dist
        };
        v[2].dist = long(x3,x4);
    }
    if(!borde_izquierdo) {
        var x3 = {
            pieceIndex: x1.pieceIndex,
            lane: x1.lane - 1,
            dist: x1.dist
        };
        var x4 = {
            pieceIndex: x2.pieceIndex,
            lane: x2.lane - 1,
            dist: x2.dist
        };
        v[0].dist = long(x3,x4);
    }
    var punt = [0.0, 0.0, 0.0];
    for(var n = 0; n < 3; n++) {
        if(!v[n].ocup) { 
            punt[n] += 2000.0;
        }
        punt[n] += 50000.0/v[n].dist;
    }
    var max = punt[1], pos = 1;
    for(var n = 0; n < 3; n++) {
        if(punt[n] > max) {
            max = punt[n];
            pos = n;
        }
    }
    if(pos == 0) {
        switchToLane = "Left";
        return true;
    }
    else if(pos == 2) {
        switchToLane = "Right";
        return true;
    }
    else {
        switchToLane = "";
        return false;
    }
}
    
function aTodoGas6(x_start) {
	if (rectaGrande) {
    	var x1 = {pieceIndex: x_start.pieceIndex,lane: x_start.lane,dist: 0};
		var j = (x1.pieceIndex+1)%le;
		while(aux2.pieces[j].hasOwnProperty("length")) j=(j+1)%le;
    	var x2 = {pieceIndex: j,lane: x1.lane,dist: 0};
    	return long(x1,x2) >= Math.max(max_long_zona_turbo-50,300);
	} else {
		var x1 = {pieceIndex: x_start.pieceIndex,lane: x_start.lane,dist: 0};
		var j = (x1.pieceIndex+1)%le;
		var bool = true;
		while(bool) {
			if(!aux2.pieces[j].hasOwnProperty("length")) {
				if(!(aux2.pieces[j].radius-aux2.pieces[j].angle/Math.abs(aux2.pieces[j].angle)*aux2.lanes[x_start.lane].distanceFromCenter>=200)) bool = false;
				else j=(j+1)%le;
			}
			else j=(j+1)%le;
			if (j == x1.pieceIndex) return true;
		}
    	var x2 = {pieceIndex: j,lane: x1.lane,dist: 0};
    	return long(x1,x2) > 25*k;
	}
}

function normal(radio,veloc,angP) {
	var D = c1*veloc+c2*veloc*veloc/Math.sqrt(radio);
	var B = b*veloc;
	var A = k1+k2*veloc;
    var temp=D/B-(angP-D/B)*(Math.pow(e,(-Math.PI/2)*A/Math.sqrt(B-A*A/4)));
	return temp;
}
function normal2(radio,veloc,tiempo,angP){
	var D = c1*veloc+c2*veloc*veloc/Math.sqrt(radio);
	var B = b*veloc;
    var A = k1+k2*veloc;
    var temp= D/B+(angP-D/B)*Math.pow(e,-A*tiempo/2)*Math.cos(tiempo*Math.sqrt(b*veloc-A*A/4));
    return temp;
}    
function vel_max_curva(i,rad,tamano,angP) { //Mejorable el caso switch antes de curva
    var curva = aux2.pieces[i];
    var res = (45-c1/b)*Math.sqrt(rad)*b/c2;
    while(normal(rad,res,angP)<59) res += 0.02;
	res -= 0.02;
	while(normal(rad,res,angP)<59) res += 0.005;
    if (tamano/res<(Math.PI/Math.sqrt(b*res-Math.pow(k1+k2*res,2)/4))) {
        while(normal2(rad,res,tamano/res,angP) < 50) res += 0.005;
    }
    return res;
}

function dist_segura(v0,vf) {
    if (v0 >= vf) return 0;
    if(vf > k) return Infinity;
    else return k/lambda*Math.log((k-v0)/(k-vf))-(k-v0)/lambda*(1-(k-vf)/(k-v0));   
}
function dist_acelerando(dist_no_segura,v0,vf) {
    var tc = (dist_no_segura-(v0-vf)/lambda)/k;
    return k*tc-(1-Math.pow(e,-lambda*tc))*(k-v0)/lambda;
}

function send_throttle(thro,time) {
	try{
    	send({
        	msgType: "throttle",
            data: Math.min(thro,1),
            gameTick: time
        });
    }catch(err){
        console.log("ERROR: ",err," at line ",err.lineNumber);
        send({
            msgType: "ping",
        	data: {}
    	});
    }
}

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function (data) {
    if (data.msgType === 'carPositions') {
        myCar = 0;
        while (myCar < data.data.length && myCarColor !== data.data[myCar].id.color) myCar++;
        if(!crashed) {
            var aux = velocity(data);
            for(var i = 0; i < infoInit.race.cars.length; i++) {
                if(data.data[i].piecePosition.lane.startLaneIndex !== data.data[i].piecePosition.lane.endLaneIndex) {
                    switched[i] = true;
                    velsw[i] = aux[i];
                }
            }
            if (midiendo > 0) { 
				if (data.gameTick == 1) {
            		for(var i = 0; i < infoInit.race.cars.length; i++) {
                		x_before[i] = {
                			pieceIndex: data.data[i].piecePosition.pieceIndex,
                    		lane: data.data[i].piecePosition.lane.startLaneIndex,
                    		dist: data.data[i].piecePosition.inPieceDistance
            			}
        			}
        		}
                if (k == 1 && data.gameTick >= 1) {  
                    if (aux[myCar] == 0) ++lambda; 
                    else k = 0;
                } else {
                    if (data.gameTick == (lambda)) {
                        midiendo = aux[myCar];
                        console.log("velocidad 5:" + midiendo);
                    }
                    if (data.gameTick == (5 + lambda)) {
                        console.log("velocidad 10:" + aux[myCar]);
                        k = midiendo / (2 - aux[myCar] / midiendo);
                        lambda = (Math.log(aux[myCar] / midiendo - 1)) / (-5);
                        console.log("k:" + k);
                        console.log("lambda:" + lambda);
                        midiendo = 0;
                    }
                }
                send_throttle(1,data.gameTick);
            } else { //TODO MEDIDO
				var lane_o=data.data[myCar].piecePosition.lane.startLaneIndex;
                var lane_f=data.data[myCar].piecePosition.lane.endLaneIndex;
				var i = data.data[myCar].piecePosition.pieceIndex;
				if (c2provi) {
					if(aux2.pieces[x_now[myCar].pieceIndex].hasOwnProperty("radius")) {
						if (data.data[myCar].angle != 0) {
                            var mult = aux2.pieces[x_now[myCar].pieceIndex].angle/Math.abs(aux2.pieces[x_now[myCar].pieceIndex].angle);;
							var rad = aux2.pieces[x_now[myCar].pieceIndex].radius-mult*aux2.lanes[lane_o].distanceFromCenter;
							c2 = Math.sqrt(rad)/(aux[myCar]*aux[myCar]);
							c2 *= ((1+k1)*Math.abs(data.data[myCar].angle)-c1*aux[myCar]);
							c2provi = false;
						}
						send_throttle(aux[myCar]/k,data.gameTick,data.gameTick);
					}
				}
                console.log("Paso: ",data.gameTick,"_________________________");
                console.log("angle: ",data.data[myCar].angle,"        vel: ",aux[myCar]);
                if (aux2.pieces[x_now[myCar].pieceIndex].hasOwnProperty("switch")) {
                    isUsefullSwitch = true;
                    switchToLane = "";
                }
                var pieceEnd= x_now[myCar].dist > long({pieceIndex:i,lane:lane_f,dist:0},{pieceIndex:(i+1)%le,lane:lane_f,dist:0})-k;
                if (!c2provi && aux2.pieces[(i+1)%le].hasOwnProperty("switch") && pieceEnd && isUsefullSwitch) {
                    if (switchLane(data,(i+1)%le,aux,velocitat)) {
                        send({
                            msgType: "switchLane",
                            data: switchToLane,
                            gameTick: data.gameTick
                        });
						calcS = false;
                    } else send_throttle(aux[myCar]/k,data.gameTick);
                    isUsefullSwitch = false;
                } else {
					var th;
                    if (aux2.pieces[i].hasOwnProperty("radius")){
						if (calcC && x_now[myCar].pieceIndex ==  x_fin_ca.pieceIndex) {
							calcC = false;
							frenar_cs = false;
						}
						var curva = aux2.pieces[i];
						var mult = curva.angle/Math.abs(curva.angle);
						if (!calcC) {
							ang_anterior = data.data[myCar].angle*mult;
							calcC = true;
						}
						console.log("ang_anterior es ",ang_anterior);
						var radio_real=curva.radius-mult*aux2.lanes[lane_f].distanceFromCenter;
                        if (lane_o != lane_f) {
                            var mult2= data.data[myCar].piecePosition.inPieceDistance/(Math.PI*Math.abs(curva.angle)/180*curva.radius);
                            radio_real+=mult*mult2*(aux2.lanes[lane_f].distanceFromCenter-aux2.lanes[lane_o].distanceFromCenter);     
                        }
                        var v_max = vel_max_curva(i,radio_real,curve_size,ang_anterior);
						if (Math.abs(data.data[myCar].angle) < ang_previo) while((c1+c2*v_max/Math.sqrt(radio_real))/b < 59) v_max+=0.003;
						velocitat = v_max;
						var fin1 = 1; 
						var find_fin = true;
						while(find_fin) {
							var curvep = aux2.pieces[(i+fin1)%le];
							if (!curvep.hasOwnProperty("radius") || curvep.radius != curva.radius) find_fin = false;
							else if (curvep.angle/Math.abs(curvep.angle)!=curva.angle/Math.abs(curva.angle)) find_fin = false;
							else ++fin1;
						}
						x_fin_ca = {pieceIndex: (i+fin1)%le,lane: lane_f,dist: 0.0};
						var j = (i+fin1)%le;
						console.log("Estoy en " +i);
						while (!aux2.pieces[j].hasOwnProperty("radius")) j = (j+1)%le; //Inicio curva siguiente
						var curva_s = aux2.pieces[j];
						var radio_cs = curva_s.radius-(curva_s.angle/Math.abs(curva_s.angle))*aux2.lanes[lane_f].distanceFromCenter;
						console.log("La siguiente curva esta en " +j);
						x_ini_cs = {pieceIndex: j,lane: lane_f,dist: 0.0};
						if (radio_real > radio_cs) {
							var fin2 = 1; //Final curva siguiente
							while(aux2.pieces[(j+fin2)%le].hasOwnProperty("radius") && aux2.pieces[(j+fin2)%le].radius ==  aux2.pieces[j].radius) ++fin2;
							var x_fin_cs = {pieceIndex: (j+fin2)%le,lane: lane_f,dist: 0.0};
							dist_frenado = (aux[myCar]-vel_max_curva(j,radio_cs,long(x_ini_cs,x_fin_cs),0))/lambda;
							frenar_cs = true;
						}
						console.log("distancia a la proxima curva " +long(x_now[myCar],x_ini_cs));
						if (frenar_cs && long(x_now[myCar],x_ini_cs) <= dist_frenado) th = 0.0;
						else {
							if (aux[myCar] <= v_max*0.98) th = 1.0;
                            else if (aux[myCar] <= v_max) th = v_max/k;
                            else th = 0.0;
						}
						ang_previo = Math.abs(data.data[myCar].angle);	
						console.log("dist_fr: " + dist_frenado);
                    } else {   //RECTAS
                        if(!calcR && turboAvailable && aTodoGas6(x_now[myCar])) {
                            k *= infoTurbo.turboFactor;
                            send({
                                msgType: "turbo",
                            	data: "I'm on a highway to hell! NA NA NA"
                            });
							calcR = true;
                        } 
						calcC = false;
						frenar_cs = false;
						var sw = 1;
                        while (!aux2.pieces[(i+sw)%le].hasOwnProperty("switch")) ++sw; //SWITCH
                        var j = 1;
                        while (aux2.pieces[(i+j)%le].hasOwnProperty("length")) ++j; //CURVA
                        var fin = j;
                        while (aux2.pieces[(i+fin)%le].hasOwnProperty("radius") && aux2.pieces[(i+fin)%le].radius==aux2.pieces[(i+j)%le].radius) ++fin;
                        var j2 = fin;
                        while (aux2.pieces[(i+j)%le].hasOwnProperty("length")) ++j2; //CURVA 2
                        if (j > sw) { //Mira la lane más corta si la curva va después que el switch
							var sw2 = sw+1;
							while (!aux2.pieces[(i+sw2)%le].hasOwnProperty("switch")) ++sw2;
							var x_s1a={pieceIndex:(i+sw)%le,lane:lane_f,dist:0.0};
							var lane2 = (infoInit.race.track.lanes.length+lane_f-1)%infoInit.race.track.lanes.length;
							var x_s1b={pieceIndex:(i+sw)%le,lane:lane2,dist:0.0};
							var x_s2={pieceIndex:(i+sw2)%le,lane:lane_f,dist:0.0};
							if (lane2 > lane_f) {
								if (long(x_s1a,x_s2) > long(x_s1b,x_s2)) lane_f = lane_f+1;
							} else if (long(x_s1a,x_s2) > long(x_s1b,x_s2)) lane_f = lane_f-1;
						}
						x_ini_cs = {pieceIndex: ((i+j)%le),lane: lane_f,dist: 0.0};
                        var x_fin_cs = {pieceIndex: ((i+fin)%le),lane: lane_f,dist: 0.0};
                        curve_size = long(x_ini_cs,x_fin_cs);
						var curva_s = aux2.pieces[(i+j)%le];
						var radio_cs = curva_s.radius-(curva_s.angle/Math.abs(curva_s.angle))*aux2.lanes[lane_f].distanceFromCenter;
						var v_max2 = 0;
						if ((j2 == fin || j2 == fin+1) && curve_size<30*k) {
							if((radio_cs-aux2.pieces[(i+j2)%le].radius)<=50) {
								console.log("gerpar");
								var curva_2 = aux2.pieces[(i+j2)%le];
								var radio_c2 = curva_2.radius-(curva_2.angle/Math.abs(curva_2.angle))*aux2.lanes[lane_f].distanceFromCenter;
								var x_ini_c2 = {pieceIndex: ((i+j2)%le),lane: lane_f,dist: 0.0};
								v_max2 = vel_max_curva((i+j2)%le,radio_c2,300,0);
								var dist_flej = (aux[myCar]-v_max2)/lambda;
							}
						}
                        v_max_rect = vel_max_curva((i+j)%le,radio_cs,curve_size,data.data[myCar].angle);
                        var dist_next_curve = long(x_now[myCar],x_ini_cs);
                        console.log("DISTANCIA A CURVA: " +dist_next_curve);
                        console.log("VEL MAX CURVA: " + v_max_rect);
						velocitat = v_max_rect*1.1;
                        dist_f_rect = Math.min(dist_segura(aux[myCar],v_max_rect),dist_next_curve);
                        var dist_no_segura = dist_next_curve-dist_f_rect;
                        if (dist_f_rect!=dist_next_curve) {
                       		if (dist_f_rect==0) dist_f_rect += dist_acelerando(dist_no_segura,aux[myCar],v_max_rect) + aux[myCar];
                            else dist_f_rect += dist_acelerando(dist_no_segura,v_max_rect,v_max_rect) + aux[myCar];
                        }
						console.log("dist_f_rect es ",dist_f_rect);
						console.log("dist otra es ",(aux[myCar]-v_max_rect)/lambda);
						if (dist_next_curve > (aux[myCar]-v_max_rect)/lambda) th = 1.0
                    	else if (aux[myCar] > v_max_rect) th = 0.0;
                    	else if (aux[myCar] <= v_max_rect*0.98) th = 1.0;
						else th = Math.min(1,v_max_rect/k);
						if (v_max2 != 0 && long(x_now[myCar],x_ini_c2)<dist_flej) th = 0.0;
                    }
					send_throttle(th,data.gameTick);
                }
            }
        } else { //Crashed
            send({
                msgType: "ping",
                data: {}
            });
        }
    } else {
        if (data.msgType === 'join') {
            console.log('Joined');
        } else if (data.msgType === 'yourCar') {
            myCarColor = data.data.color;
        } else if (data.msgType === 'gameInit') {
			infoInit = data.data;
			aux2 = infoInit.race.track;
			le = aux2.pieces.length;
			x_before = new Array(infoInit.race.cars.length);
            x_now = new Array(infoInit.race.cars.length);
            switched = new Array(infoInit.race.cars.length);
            velsw = new Array(infoInit.race.cars.length);
			for(var i = 0; i < infoInit.race.cars.length; i++) {
                x_before[i] = {
                    pieceIndex: 0,
                    lane: 0,
                    dist: 0
                };
                switched[i] = false;
				x_now[i] = {
				    pieceIndex: null,
                    lane: null,
                    dist: null
                };
            }
			console.log(data.data.race.track.lanes);
            var long_zona_turbo = 0.0;
            var long_inicial = 0.0;
            var long_final = 0.0;
            var it = 0;
            while(it < aux2.pieces.length && aux2.pieces[it].hasOwnProperty("length")) { //mide longitud tramo inicial
                long_inicial += aux2.pieces[it].length;
                it++;
            }
            console.log("long_incial: " + long_inicial);
            max_long_zona_turbo = long_inicial;
            while(it < aux2.pieces.length) {
            	if(aux2.pieces[it].hasOwnProperty("length")) long_zona_turbo += aux2.pieces[it].length;
            	else {
                	if(long_zona_turbo > max_long_zona_turbo) max_long_zona_turbo = long_zona_turbo;
                	long_zona_turbo = 0.0;
            	}
            	it++;
            }
            console.log("max: " + max_long_zona_turbo);
            it = aux2.pieces.length-1;
            while(it > 0 && aux2.pieces[it].hasOwnProperty("length")) { //mide longitud tramo final
                long_final += aux2.pieces[it].length;
                it--;
            }
            console.log("long_final: " + long_final);
            if(long_inicial + long_final > max_long_zona_turbo) max_long_zona_turbo = long_inicial + long_final;
			if (max_long_zona_turbo < 300) rectaGrande = false;
        } else if (data.msgType === 'gameStart') {
            send({
                msgType: "ping",
                data: {}
            });
            console.log('Race started');
        } else if (data.msgType === 'gameEnd') {
            console.log('Race ended');
        } else if (data.msgType === 'crash' && data.data.color === myCarColor){
            console.log("crash");
            crashed = true;
			if (k >= 20) k/=infoTurbo.turboFactor;
            send({
                msgType: "ping",
                data: {},
                gameTick: data.gameTick
            });
		} else if(data.msgType === 'turboAvailable') {
            infoTurbo = data.data;
            turboAvailable = true;
		} else if(data.msgType === 'turboEnd' && data.data.color === myCarColor) {
            k /= infoTurbo.turboFactor;
            turboAvailable = false;
        } else if (data.msgType === 'spawn' && data.data.color === myCarColor) {
            crashed = false;
            send({
                msgType: "throttle",
                data: 1.0,
                gameTick: data.gameTick
            }); //I'm on a highway to hell! NA NA NA
		} else if (data.msgType === 'error' || data.msgType === 'dnf'){
			console.log("_________");
			console.log(data);
			console.log("_________");
        } else {
            console.log('other type of msg:', data.msgType);
            send({
                msgType: "ping",
                data: {}
            });
        }
    }
});
jsonStream.on('error', function () {
    return console.log("disconnected");
});